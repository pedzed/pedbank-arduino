#include <cantino.h>
#include <Keypad.h>
#include <SPI.h>
#include <MFRC522.h>

using namespace cantino;

using Rfid = MFRC522;

void printKeyOnKeypadPress(Keypad);
void printCardIdOnInsert(Rfid);
String rfidCardUid(int bufferSize, byte *buffer);

int main()
{
    cout << "Program started." << endl;

    const int RFID_SDA_PIN = 10;
    const int RFID_RST_PIN = 9;

    const int KEYPAD_ROWS = 4;
    const int KEYPAD_COLS = 4;

    char keys[KEYPAD_ROWS][KEYPAD_COLS] = {
        {'1','2','3','A'},
        {'4','5','6','B'},
        {'7','8','9','C'},
        {'*','0','#','D'}
    };

    // byte rowPins[KEYPAD_ROWS] = {44, 42, 40, 38};
    // byte colPins[KEYPAD_COLS] = {36, 34, 32, 30};

    byte rowPins[KEYPAD_ROWS] = {A0, 2, 3, 4};
    byte colPins[KEYPAD_COLS] = {8, 7, 6, 5};

    Keypad keypad = Keypad(
        makeKeymap(keys),
        rowPins,
        colPins,
        KEYPAD_ROWS,
        KEYPAD_COLS
    );

    SPI.begin();

    Rfid rfid(RFID_SDA_PIN, RFID_RST_PIN);
    rfid.PCD_Init();

    while (true) {
        printKeyOnKeypadPress(keypad);
        printCardIdOnInsert(rfid);
    }

    return 0;
}

void printKeyOnKeypadPress(Keypad keypad)
{
    char key = keypad.getKey();

    if (key == NO_KEY) {
        return;
    }

    cout << "[KEYPAD]: " << key << endl;
    delay(400);
}

void printCardIdOnInsert(Rfid rfid)
{
    if (!rfid.PICC_IsNewCardPresent()) {
        return;
    }

    if (!rfid.PICC_ReadCardSerial()){
        return;
    }

    cout << "[RFID]: ";
    Serial.print(rfidCardUid(rfid.uid.size, rfid.uid.uidByte));
    cout << endl;

    rfid.PICC_HaltA();
    rfid.PCD_StopCrypto1();
}

String rfidCardUid(int bufferSize, byte *buffer) {
    String uid = "";

    for (int i = 0; i < bufferSize; i++) {
        uid += String(buffer[i] < 0x10 ? "0" : "");
        uid += String(buffer[i], HEX);
    }

    uid.trim();

    return uid;
}
